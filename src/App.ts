import config from 'config'
import express from 'express'
import bodyParser from 'body-parser'
import cors from 'cors'

import { errorHandler } from 'middlewares'
import { Database } from 'services'
import { routes } from 'routes'
import { Passport } from 'services/Passport'

const port = config.get('port')

Database.connect()

const app = express()

Passport.config()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(cors())
app.use(routes)

app.use(errorHandler)

app.listen(port, () => {
  // eslint-disable-next-line no-console
  console.log(`${config.get('env')} server listening on port ${port}`)
})
