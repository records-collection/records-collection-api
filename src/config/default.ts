export default {
  port: process.env.PORT ?? 8100,
  env: process.env.NODE_ENV ?? 'development',
  dbHost: process.env.DB_HOST,
  discogs: {
    apiUrl: process.env.DISCOGS_API,
    consumerKey: process.env.DISCOGS_CONSUMER_KEY,
    consumerSecretKey: process.env.DISCOGS_CONSUMER_SECRET_KEY,
  },
  jwtSecret: process.env.JWT_SECRET,
  smtp: {
    server: process.env.SMTP_SERVER,
    user: process.env.SMTP_USER,
    password: process.env.SMTP_PASSWORD,
  },
  aws: {
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    bucketName: process.env.AWS_BUCKET_NAME,
  },
}
