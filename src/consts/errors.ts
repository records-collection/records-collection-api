const ERRORS = {
  GENERIC_ERROR: 'GenericError',
  VALIDATION_ERROR: 'ValidationError',
  AUTHENTICATION_ERROR: 'AuthenticationError',
  DUPLICATE_KEY_ERROR: 'DuplicateKeyError',
  ACCOUNT_NOT_CONFIRMED_ERROR: 'AccountNotConfirmedError',
}

export { ERRORS }
