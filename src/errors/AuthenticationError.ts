import { ERRORS } from 'consts'

import { GenericError } from './GenericError'

class AuthenticationError extends GenericError {
  constructor(message: string) {
    super(400, message, ERRORS.AUTHENTICATION_ERROR)
  }
}

export { AuthenticationError }
