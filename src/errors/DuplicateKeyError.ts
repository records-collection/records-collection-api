import { ERRORS } from 'consts'

import { GenericError } from './GenericError'

class DuplicateKeyError extends GenericError {
  duplicatekey: string

  constructor(duplicatekey: string) {
    super(400, ERRORS.DUPLICATE_KEY_ERROR, ERRORS.DUPLICATE_KEY_ERROR)
    this.duplicatekey = duplicatekey
  }
}

export { DuplicateKeyError }
