import { ERRORS } from 'consts'

class GenericError extends Error {
  type: string

  status: number

  constructor(status: number, message: string, type = ERRORS.GENERIC_ERROR) {
    super(message)
    this.type = type
    this.status = status
  }
}

export { GenericError }
