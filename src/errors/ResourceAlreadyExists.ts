import { GenericError } from './GenericError'

class ResourceAlreadyExists extends GenericError {
  constructor(resource: string) {
    super(404, `${resource} already exists`, 'ResourceAlreadyExists')
  }
}

export { ResourceAlreadyExists }
