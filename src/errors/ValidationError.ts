import { ERRORS } from 'consts'

import { GenericError } from './GenericError'

class ValidationError extends GenericError {
  constructor(message: string) {
    super(400, message, ERRORS.VALIDATION_ERROR)
  }
}

export { ValidationError }
