import S3, { ListObjectsV2Output, DeleteObjectOutput } from 'aws-sdk/clients/s3'
import config from 'config'
import { v4 as uuid } from 'uuid'
import { PromiseResult } from 'aws-sdk/lib/request'
import { AWSError } from 'aws-sdk/lib/error'

interface AWSConfig {
  accessKeyId: string
  secretAccessKey: string
  bucketName: string
}

const { accessKeyId, secretAccessKey, bucketName } = config.get<AWSConfig>('aws')

const s3 = new S3({
  region: 'us-east-2',
  signatureVersion: 'v4',
  apiVersion: '2006-03-01',
  credentials: {
    accessKeyId,
    secretAccessKey,
  },
})

const listObjects = async (): Promise<PromiseResult<ListObjectsV2Output, AWSError>> => {
  return s3.listObjectsV2({ Bucket: bucketName }).promise()
}

const deleteObject = async (key: string): Promise<PromiseResult<DeleteObjectOutput, AWSError>> => {
  return s3.deleteObject({ Bucket: bucketName, Key: key }).promise()
}

const createObject = async (type: string, file: Buffer): Promise<string> => {
  const key = uuid()
  await s3
    .putObject({
      Bucket: bucketName,
      Key: key,
      ContentType: type,
      Body: file,
    })
    .promise()
  return key
}

const getSignedUrl = async (key: string): Promise<string> => {
  return s3.getSignedUrlPromise('getObject', { Bucket: bucketName, Key: key })
}

const putObject = async (key: string, type: string, file: Buffer): Promise<void> => {
  await s3
    .putObject({
      Bucket: bucketName,
      Key: key,
      ContentType: type,
      Body: file,
    })
    .promise()
}

export const AmazonS3Service = {
  listObjects,
  deleteObject,
  putObject,
  getSignedUrl,
  createObject,
}
