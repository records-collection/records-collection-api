import { AuthenticationError } from 'errors'
import { UserService } from 'features/users'
import { AuthenticatedUser } from 'types'
import { AuthUtils } from 'utils/auth'

const authenticate = async (username: string, password: string): Promise<AuthenticatedUser> => {
  const user = await UserService.findByUsernameOrEmail(username)

  if (!user) throw new AuthenticationError('User not found')

  if (!user?.password) throw new AuthenticationError('Login with social network')

  const matchPassword = await AuthUtils.comparePassword(password, user.password)

  if (!matchPassword) throw new AuthenticationError('Incorrect password')

  return { id: user.id, username: user.username, email: user.email, picture: user.picture }
}

const findAuthenticatedUser = async (id: string): Promise<AuthenticatedUser> => {
  const user = await UserService.findById(id)

  if (!user) throw new AuthenticationError('User not found')

  return { id: user.id, email: user.email, username: user.username, picture: user.picture }
}

export const AuthenticationService = {
  authenticate,
  findAuthenticatedUser,
}
