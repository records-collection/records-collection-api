/* eslint-disable no-param-reassign */
import mongoose, { Document, Schema } from 'mongoose'

const MessageSchema = new Schema({
  createdAt: {
    type: Date,
    default: new Date(),
  },
  message: {
    type: String,
    required: true,
  },
  user: {
    required: true,
    type: Schema.Types.ObjectId,
    ref: 'User',
  },
  postId: {
    type: String,
    required: false,
  },
  postPicture: {
    type: String,
    required: false,
  },
  postTitle: {
    type: String,
    require: false,
  },
  type: {
    required: true,
    type: String,
    enum: ['Message', 'Reservation', 'Sold', 'Canceled'],
    default: 'Message',
  },
})

const ChatSchema = new Schema(
  {
    user1: {
      required: true,
      type: Schema.Types.ObjectId,
      ref: 'User',
    },
    user2: {
      required: true,
      type: Schema.Types.ObjectId,
      ref: 'User',
    },
    messages: {
      type: [MessageSchema],
      default: [],
    },
  },
  {
    collection: 'chats',
  },
)

ChatSchema.set('timestamps', true)
MessageSchema.set('timestamps', true)

ChatSchema.set('toObject', {
  transform: (_, ret) => {
    ret.id = ret._id
    delete ret._id
    // eslint-disable-next-line no-underscore-dangle
    delete ret.__v
  },
})

interface IChat extends Document {
  id: string
  createdAt: Date
  user1: string
  user2: string
}

const ChatModel = mongoose.model<IChat>('Chat', ChatSchema)

export { ChatModel, IChat }
