import { ResourceNotFound } from 'errors'

import { ChatModel, IChat } from '../models'
import { CreateChat, CreateMessage } from '../types'

const getChatsByUserId = async (userId: string) => {
  return ChatModel.find({ $or: [{ user1: userId }, { user2: userId }] })
    .sort('-updatedAt')
    .populate({ path: 'user1', select: 'id username picture' })
    .populate({ path: 'user2', select: 'id username picture' })
    .exec()
}

const getOne = async (chatId: string): Promise<IChat> => {
  try {
    const release = await ChatModel.findById(chatId)
      .populate({ path: 'user1', select: 'id username picture' })
      .populate({ path: 'user2', select: 'id username picture' })
      .exec()
    if (release) return release
    throw new ResourceNotFound('Chat')
  } catch (error) {
    throw new ResourceNotFound('Chat')
  }
}

const create = async (chat: CreateChat): Promise<IChat> => {
  const chatFound = await ChatModel.findOne({
    $or: [
      { user1: chat.user1, user2: chat.user2 },
      { user1: chat.user2, user2: chat.user1 },
    ],
  })
    .populate({ path: 'user1', select: 'id username picture' })
    .populate({ path: 'user2', select: 'id username picture' })
    .exec()

  if (chatFound) return chatFound

  const chatModel = new ChatModel(chat)
  return chatModel.save()
}

const addMessage = async (chatId: string, payload: CreateMessage, user: Express.User) => {
  const message = {
    ...payload,
    user: user.id,
  }

  return ChatModel.updateOne({ _id: chatId }, { $addToSet: { messages: message } })
}

export const ChatService = {
  getChatsByUserId,
  getOne,
  create,
  addMessage,
}
