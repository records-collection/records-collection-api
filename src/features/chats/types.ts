export interface Chat {
  id: string
  user1: string
  user2: string
}

export type CreateChat = Omit<Chat, 'id'>

export interface CreateMessage {
  message: string
  type?: string
  postId?: string
  postPicture?: string
  postTitle?: string
}
