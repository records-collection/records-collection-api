import mongoose, { Document } from 'mongoose'

import { ReleaseSchema } from './ReleaseModel'

const { Schema } = mongoose

const CollectionSchema = new Schema(
  {
    wishList: {
      type: [ReleaseSchema],
      default: [],
      required: true,
    },
  },
  {
    collection: 'collections',
  },
)

CollectionSchema.set('timestamps', true)

interface ICollection extends Document {
  id: string
}

const CollectionModel = mongoose.model<ICollection>('Collection', CollectionSchema)

export { CollectionModel }
