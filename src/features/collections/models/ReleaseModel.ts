import mongoose from 'mongoose'

const { Schema } = mongoose

const IdentifierSchema = new Schema(
  {
    type: {
      type: String,
    },
    value: {
      type: String,
    },
  },
  { _id: false },
)

const ArtistSchema = new Schema(
  {
    type: {
      type: String,
    },
    anv: {
      type: String,
    },
    discogsId: {
      type: Number,
    },
    join: {
      type: String,
    },
    name: {
      type: String,
    },
    resourceUrl: {
      type: String,
    },
    role: {
      type: String,
    },
    tracks: {
      type: String,
    },
  },
  { _id: false },
)

const LabelSchema = new Schema(
  {
    catno: {
      type: String,
    },
    discogsId: {
      type: Number,
    },
    name: {
      type: String,
    },
    resourceUrl: {
      type: String,
    },
  },
  { _id: false },
)

const ImageSchema = new Schema(
  {
    resourceUrl: {
      type: String,
    },
    type: {
      type: String,
    },
    uri: {
      type: String,
    },
    uri150: {
      type: String,
    },
    height: {
      type: Number,
    },
    width: {
      type: Number,
    },
  },
  { _id: false },
)

const FormatSchema = new Schema(
  {
    name: {
      type: String,
    },
    qty: {
      type: String,
    },
    descriptions: {
      type: [String],
    },
  },
  { _id: false },
)

const ReleaseSchema = new Schema(
  {
    title: {
      type: String,
    },
    discogsId: {
      type: Number,
    },
    country: {
      type: String,
    },
    genres: {
      type: [String],
    },
    year: {
      type: Number,
    },
    identifiers: {
      type: [IdentifierSchema],
    },
    images: {
      type: [ImageSchema],
    },
    labels: {
      type: [LabelSchema],
    },
    artists: {
      type: [ArtistSchema],
    },
    formats: {
      type: [FormatSchema],
    },
  },
  {
    _id: false,
    collection: 'releases',
  },
)

export { ReleaseSchema }
