import { Document, Types } from 'mongoose'

import { CollectionModel } from '../models'
import { AddReleasePayload } from '../types'

const findCollectionById = async (id: string) => {
  return CollectionModel.findOne({ _id: id })
}

const getWishList = async (id: string, search: string) => {
  return CollectionModel.aggregate([
    {
      $match: { _id: new Types.ObjectId(id) },
    },
    {
      $project: {
        releases: {
          $filter: {
            input: '$wishList',
            as: 'wishList',
            cond: {
              $regexMatch: {
                input: '$$wishList.title',
                regex: search,
                options: 'i',
              },
            },
          },
        },
      },
    },
  ])
}

const create = async (): Promise<Document> => {
  const collectionModel = new CollectionModel({})
  return collectionModel.save()
}

const deleteById = async (id: string) => {
  return CollectionModel.deleteOne({ _id: id })
}

const addReleaseToWishList = async (payload: AddReleasePayload) => {
  return CollectionModel.updateOne(
    { _id: payload.collectionId },
    { $addToSet: { wishList: payload.release } },
  )
}

const removeReleaseFromWishList = async (collectionId: string, releaseId: number) => {
  return CollectionModel.updateOne(
    { _id: collectionId },
    { $pull: { wishList: { discogsId: releaseId } } },
  )
}

export const CollectionService = {
  findCollectionById,
  create,
  deleteById,
  getWishList,
  addReleaseToWishList,
  removeReleaseFromWishList,
}
