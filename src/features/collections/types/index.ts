export interface AddReleasePayload {
  collectionId: string
  release: ReleasePayload
}

export interface ReleasePayload {
  title: string
  id: number
}
