import { Discogs } from 'services'

const findReleaseById = (id: string) => {
  return Discogs.get(`/releases/${id}`)
}

const findArtistById = (id: string) => {
  return Discogs.get(`/artists/${id}`)
}

const findReleasesByArtistId = (id: string) => {
  return Discogs.get(`/artists/${id}/releases`)
}

const findMasterById = (id: string) => {
  return Discogs.get(`/masters/${id}`)
}

const findMastersByArtistId = (id: string) => {
  const params = {
    type: 'master',
  }
  return Discogs.get(`/artists/${id}/releases`, { params })
}

interface Params {
  page?: string
  pageSize?: string
}

const findVersionsByMasterId = (masterId: string, versionParams?: Params) => {
  const params = {
    page: versionParams?.page ?? 0,
    per_page: versionParams?.pageSize ?? 10,
  }
  return Discogs.get(`/masters/${masterId}/versions`, { params })
}

const search = (
  query: string,
  type?: string,
  year?: string,
  format?: string,
  country?: string,
  pageSize?: number,
  page?: number,
) => {
  const params = {
    query,
    per_page: pageSize ?? 10,
    page: page ?? 0,
    type: type ?? '',
    year: year ?? '',
    format: format ?? '',
    country: country ?? '',
  }

  return Discogs.get('/database/search', { params })
}

export const DiscogsService = {
  findReleaseById,
  findArtistById,
  search,
  findReleasesByArtistId,
  findMastersByArtistId,
  findMasterById,
  findVersionsByMasterId,
}
