export interface DiscogsReleaseResponse {
  id: number
  year: number
  title: string
  country: string
  released: string
}
