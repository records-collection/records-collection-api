/* eslint-disable no-param-reassign */
import mongoose, { Document, Schema } from 'mongoose'

import { IRelease } from 'features/releases/models'

const CommentSchema = new Schema({
  createdAt: {
    type: Date,
    default: new Date(),
  },
  comment: {
    type: String,
    required: true,
  },
  userId: {
    type: String,
    required: true,
  },
  username: {
    type: String,
    required: true,
  },
  userPicture: {
    type: String,
  },
})

const PostSchema = new Schema(
  {
    price: {
      type: Number,
      required: true,
    },
    state: {
      type: String,
      enum: ['Reserved', 'Available', 'Sold'],
      default: 'Available',
    },
    notes: {
      type: String,
      required: false,
    },
    release: {
      required: true,
      type: Schema.Types.ObjectId,
      ref: 'Release',
    },
    user: {
      required: true,
      type: Schema.Types.ObjectId,
      ref: 'User',
    },
    likes: {
      type: [String],
      default: [],
    },
    comments: {
      type: [CommentSchema],
      default: [],
    },
    reservedBy: {
      type: Schema.Types.ObjectId,
      ref: 'User',
    },
    soldTo: {
      type: Schema.Types.ObjectId,
      ref: 'User',
    },
  },
  {
    collection: 'posts',
  },
)

PostSchema.set('timestamps', true)
CommentSchema.set('timestamps', true)

PostSchema.set('toObject', {
  transform: (_, ret) => {
    ret.id = ret._id
    delete ret._id
    // eslint-disable-next-line no-underscore-dangle
    delete ret.__v
  },
})

interface IPost extends Document {
  id: string
  state: string
  notes?: string
  createdAt: Date
  release: IRelease
  user: string
  likes: string[]
  reservedBy?: string
  soldTo?: string
}

const PostModel = mongoose.model<IPost>('Post', PostSchema)

export { PostModel, IPost }
