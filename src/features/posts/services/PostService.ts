import { ResourceNotFound } from 'errors'
import { ResourceAlreadyExists } from 'errors/ResourceAlreadyExists'
import { ChatService } from 'features/chats'
import { ReleaseService } from 'features/releases'
import { UserModel, UserService } from 'features/users'

import { PostModel, IPost } from '../models'
import { CreateCommentPayload, CreatePostPayload, UpdatePostPayload } from '../types'

const getAll = async () => {
  return PostModel.find()
    .sort('-createdAt')
    .populate('release')
    .populate({ path: 'user', select: 'id username email' })
    .exec()
}

const getOne = async (id: string): Promise<IPost> => {
  try {
    const release = await PostModel.findById(id)
      .populate('release')
      .populate({ path: 'user', select: 'id username email' })
      .populate({ path: 'reservedBy', select: 'id username email' })
      .populate({ path: 'soldTo', select: 'id username email' })
      .exec()
    if (release) return release
    throw new ResourceNotFound('Post')
  } catch (error) {
    throw new ResourceNotFound('Post')
  }
}

const create = async (
  payload: CreatePostPayload,
  images?:
    | Express.Multer.File[]
    | {
        [fieldname: string]: Express.Multer.File[]
      },
): Promise<IPost> => {
  const { releaseUpdate, ...post } = payload

  // eslint-disable-next-line @typescript-eslint/ban-ts-comment
  // @ts-ignore
  const oldPost = await PostModel.findOne({ release: post.release }).populate('release').exec()

  if (oldPost && oldPost.state !== 'Sold') {
    throw new ResourceAlreadyExists('Post')
  }

  if (releaseUpdate) {
    await ReleaseService.update(post.release, releaseUpdate, images)
  }

  const postModel = new PostModel(post)
  return postModel.save()
}

const update = async (
  id: string,
  payload: UpdatePostPayload,
  user: Express.User,
  images?:
    | Express.Multer.File[]
    | {
        [fieldname: string]: Express.Multer.File[]
      },
): Promise<IPost> => {
  const { releaseUpdate, ...restPost } = payload
  try {
    if (releaseUpdate) {
      await ReleaseService.update(releaseUpdate.releaseId, releaseUpdate, images)
    }

    const post = await PostModel.findOneAndUpdate({ _id: id }, restPost, { new: true })
      .populate('release')
      .exec()

    if (post && post.state === 'Available') {
      const { reservedBy } = post
      post.set('reservedBy', undefined, { strict: false })
      post.save()
      if (reservedBy) {
        const chat = await ChatService.create({ user1: post.user, user2: reservedBy })
        ChatService.addMessage(
          chat.id,
          {
            message: '',
            type: 'Canceled',
            postId: post.id,
            postTitle: post.release.title,
            postPicture: post.release.images[0]?.uri,
          },
          user,
        )
      }
    }

    if (post && post.state === 'Sold') {
      const { reservedBy } = post
      post.save()
      if (reservedBy) {
        const chat = await ChatService.create({ user1: post.user, user2: reservedBy })
        ChatService.addMessage(
          chat.id,
          {
            message: '',
            type: 'Sold',
            postId: post.id,
            postTitle: post.release.title,
            postPicture: post.release.images[0]?.uri,
          },
          user,
        )
        const newOwner = await UserService.findById(reservedBy)
        if (newOwner) {
          await ReleaseService.update(post.release.id, {
            userId: reservedBy,
            collectionId: newOwner.recordCollection,
          })
        }
      }
    }

    if (post) return post

    throw new ResourceNotFound('Post')
  } catch (error) {
    throw new ResourceNotFound('Post')
  }
}

const deleteOne = async (id: string) => {
  return PostModel.findOneAndDelete({ _id: id })
}

const createComment = async (postId: string, payload: CreateCommentPayload, user: Express.User) => {
  const comment = {
    ...payload,
    userId: user.id,
    userPicture: user.picture,
    username: user.username,
  }
  return PostModel.updateOne({ _id: postId }, { $addToSet: { comments: comment } })
}

const like = async (id: string, user: Express.User) => {
  try {
    const post = await PostModel.findOneAndUpdate({ _id: id }, { $addToSet: { likes: user.id } })
    if (!post) throw new ResourceNotFound('Post')
    return post
  } catch (error) {
    throw new ResourceNotFound('Post')
  }
}

const unlike = async (id: string, user: Express.User): Promise<IPost> => {
  try {
    const post = await PostModel.findOneAndUpdate({ _id: id }, { $pull: { likes: user.id } })
    if (!post) throw new ResourceNotFound('Post')
    return post
  } catch (error) {
    throw new ResourceNotFound('Post')
  }
}

const reserve = async (postId: string, user: Express.User): Promise<IPost> => {
  try {
    const post = await PostModel.findOneAndUpdate(
      { _id: postId },
      { reservedBy: user.id, state: 'Reserved' },
    )
      .populate('release')
      .exec()

    if (!post) throw new ResourceNotFound('Post')

    const chat = await ChatService.create({ user1: post.user, user2: user.id })
    ChatService.addMessage(
      chat.id,
      {
        message: '',
        type: 'Reservation',
        postId: post.id,
        postTitle: post.release.title,
        postPicture: post.release.images[0]?.uri,
      },
      user,
    )
    return post
  } catch (error) {
    throw new ResourceNotFound('Post')
  }
}

const save = async (postId: string, user: Express.User) => {
  try {
    await UserModel.updateOne({ _id: user.id }, { $addToSet: { postsSaved: postId } })
  } catch (error) {
    throw new ResourceNotFound('Post')
  }
}

const removeSaved = async (postId: string, user: Express.User) => {
  try {
    await UserModel.updateOne({ _id: user.id }, { $pull: { postsSaved: postId } })
  } catch (error) {
    throw new ResourceNotFound('Post')
  }
}

const postsSaved = async (user: Express.User) => {
  try {
    const result = await UserModel.findById(user.id)
      .populate({
        path: 'postsSaved',
        populate: {
          path: 'release',
          model: 'Release',
        },
      })
      .exec()
    if (!result) throw new ResourceNotFound('User')
    return result
  } catch (error) {
    throw new ResourceNotFound('User')
  }
}

const getAllByUser = async (userId: string, state: string) => {
  return PostModel.find({ user: userId, ...(state ? { state } : {}) })
    .sort('-createdAt')
    .populate('release')
    .populate({ path: 'user', select: 'id username email' })
    .exec()
}

export const PostService = {
  getAll,
  getOne,
  create,
  update,
  deleteOne,
  createComment,
  like,
  unlike,
  reserve,
  save,
  removeSaved,
  postsSaved,
  getAllByUser,
}
