import { UpdateRelease } from 'features/releases/types'

export interface Post {
  id: string
  state: string
  notes?: string
  createdAt: Date
  release: string
  user: string
  price: string
}

export interface Comment {
  id: string
  comment: string
  createdAt: Date
}

export type CreatePostPayload = Omit<Post, 'id' | 'createdAt'> & { releaseUpdate: UpdateRelease }

export interface UpdatePostPayload {
  notes?: string
  price: string
  state?: string
  reservedBy?: string
  soldTo?: string
  releaseUpdate?: {
    releaseId: string
    mediaState?: string
    supportState?: string
    personalNotes?: string
    imagesKeys?: string[]
  }
}

export type CreateCommentPayload = Omit<Comment, 'id' | 'createdAt'>
