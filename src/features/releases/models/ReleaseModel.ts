/* eslint-disable no-param-reassign */
import mongoose, { Document, Schema } from 'mongoose'

import { Artist, Format, Identifier, Image, Label } from '../types'

const IdentifierSchema = new Schema({
  type: {
    type: String,
  },
  value: {
    type: String,
  },
})

const ArtistSchema = new Schema({
  type: {
    type: String,
  },
  anv: {
    type: String,
  },
  discogsId: {
    type: Number,
  },
  join: {
    type: String,
  },
  name: {
    type: String,
  },
  resourceUrl: {
    type: String,
  },
  role: {
    type: String,
  },
  tracks: {
    type: String,
  },
})

const LabelSchema = new Schema({
  catno: {
    type: String,
  },
  discogsId: {
    type: Number,
  },
  name: {
    type: String,
  },
  resourceUrl: {
    type: String,
  },
})

const ImageSchema = new Schema({
  resourceUrl: {
    type: String,
  },
  type: {
    type: String,
  },
  uri: {
    type: String,
  },
  uri150: {
    type: String,
  },
  height: {
    type: Number,
  },
  width: {
    type: Number,
  },
})

const FormatSchema = new Schema({
  name: {
    type: String,
  },
  qty: {
    type: String,
  },
  descriptions: {
    type: [String],
  },
})

const ReleaseSchema = new Schema(
  {
    title: {
      required: true,
      type: String,
    },
    discogsId: {
      required: true,
      type: Number,
    },
    country: {
      type: String,
    },
    genres: {
      type: [String],
    },
    year: {
      type: Number,
    },
    identifiers: {
      type: [IdentifierSchema],
    },
    images: {
      type: [ImageSchema],
    },
    labels: {
      type: [LabelSchema],
    },
    artists: {
      type: [ArtistSchema],
    },
    formats: {
      type: [FormatSchema],
    },
    mediaState: {
      type: String,
    },
    supportState: {
      type: String,
    },
    personalNotes: {
      type: String,
    },
    collectionId: {
      required: true,
      type: Schema.Types.ObjectId,
      ref: 'Collection',
    },
    userId: {
      required: true,
      type: Schema.Types.ObjectId,
      ref: 'User',
    },
    imagesKeys: {
      type: [String],
      default: [],
    },
  },
  {
    collection: 'releases',
  },
)

ReleaseSchema.set('timestamps', true)

ReleaseSchema.set('toObject', {
  transform: (_, ret) => {
    ret.id = ret._id
    delete ret._id
    // eslint-disable-next-line no-underscore-dangle
    delete ret.__v
  },
})

interface IRelease extends Document {
  id: string
  title: string
  discogsId: number
  country: string
  genres: string
  year: number
  identifiers: Identifier[]
  images: Image[]
  labels: Label[]
  artists: Artist[]
  formats: Format[]
  sell: string
  exchange: string
  mediaState: string
  supportState: string
  personalNotes: string
  collectionId: string
  userId: string
  imagesKeys: string[]
}

const ReleaseModel = mongoose.model<IRelease>('Release', ReleaseSchema)

export { ReleaseModel, IRelease }
