import { FilterQuery } from 'mongoose'

import { ResourceNotFound } from 'errors'
import { AmazonS3Service } from 'features/amazon'

import { ReleaseModel, IRelease } from '../models'
import { CreateRelease, UpdateRelease } from '../types'

const findAllByCollectionId = async (
  collectionId: string,
  size: number,
  page: number,
  query: FilterQuery<IRelease>,
) => {
  return ReleaseModel.find(query)
    .limit(size)
    .skip(page * size)
    .sort({ createdAt: -1 })
}

const count = async (query: FilterQuery<IRelease>) => {
  return ReleaseModel.count(query)
}

const findById = async (id: string): Promise<IRelease> => {
  try {
    const release = await ReleaseModel.findById(id)
    if (release) return release
    throw new ResourceNotFound('Release')
  } catch (error) {
    throw new ResourceNotFound('Release')
  }
}

const create = async (
  release: CreateRelease,
  images:
    | Express.Multer.File[]
    | {
        [fieldname: string]: Express.Multer.File[]
      },
): Promise<IRelease> => {
  let imagesKeys: string[] = []
  if (Array.isArray(images) && images.length) {
    imagesKeys = await Promise.all(
      images.map((image) => AmazonS3Service.createObject(image.mimetype, image.buffer)),
    )
  }

  const releaseModel = new ReleaseModel({ ...release, imagesKeys })
  return releaseModel.save()
}

const update = async (
  id: string,
  updateRelease: UpdateRelease,
  images?:
    | Express.Multer.File[]
    | {
        [fieldname: string]: Express.Multer.File[]
      },
): Promise<IRelease> => {
  try {
    let newImagesKeys: string[] = []
    if (Array.isArray(images) && images.length) {
      newImagesKeys = await Promise.all(
        images.map((image) => AmazonS3Service.createObject(image.mimetype, image.buffer)),
      )
    }

    const payload = {
      ...updateRelease,
      imagesKeys: [...(updateRelease.imagesKeys ?? []), ...newImagesKeys],
    }

    const release = await ReleaseModel.findOneAndUpdate({ _id: id }, payload, { new: true })
    if (release) return release
    throw new ResourceNotFound('Release')
  } catch (error) {
    throw new ResourceNotFound('Release')
  }
}

const deleteById = async (id: string) => {
  return ReleaseModel.findOneAndDelete({ _id: id })
}

export const ReleaseService = {
  findAllByCollectionId,
  findById,
  create,
  update,
  deleteById,
  count,
}
