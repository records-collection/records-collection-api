export interface Release {
  id: string
  title: string
  discogsId: number
  country: string
  genres: string[]
  year: number
  identifiers: Identifier[]
  images: Image[]
  labels: Label[]
  artists: Artist[]
  formats: Format[]
  collectionId: string
  userId: string
  mediaState?: string
  supportState?: string
  personalNotes?: string
  imagesKeys?: string[]
}

export interface Identifier {
  type: string
  value: string
}

export interface Image {
  resourceUrl: string
  type: string
  uri: string
  uri150: string
  height: number
  width: number
}

export interface Format {
  name: string
  qty: string
  descriptions: string[]
}

export interface Label {
  catno: string
  discogsId: number
  name: string
  resourceUrl: string
}

export interface Artist {
  type?: string
  anv: string
  discogsId: number
  join: string
  name: string
  resourceUrl: string
  role: string
  tracks: string
}

export type CreateRelease = Omit<Release, 'id'>

export interface UpdateRelease {
  mediaState?: string
  supportState?: string
  personalNotes?: string
  imagesKeys?: string[]
  userId?: string
  collectionId?: string
}
