import mongoose, { Document } from 'mongoose'
import { nanoid } from 'nanoid'

import { AuthUtils } from 'utils/auth'

const { Schema } = mongoose

const UserSchema = new Schema(
  {
    username: {
      type: String,
      required: true,
      unique: true,
    },
    // TODO: validate valid email
    email: {
      type: String,
      required: true,
      unique: true,
    },
    recordCollection: {
      type: Schema.Types.ObjectId,
      ref: 'Collection',
    },
    password: {
      type: String,
      required: true,
    },
    provider: {
      type: String,
      default: 'local',
      required: true,
    },
    activationAccountCode: {
      type: String,
      default: nanoid(6),
      required: true,
    },
    emailConfirmed: {
      type: Boolean,
      default: false,
      required: true,
    },
    picture: {
      type: String,
      required: false,
    },
    postsSaved: [{ type: Schema.Types.ObjectId, ref: 'Post' }],
  },
  {
    collection: 'users',
  },
)

// eslint-disable-next-line func-names
UserSchema.pre<IUser>('save', async function (this, next) {
  if (!this.isModified('password')) return next()

  this.password = await AuthUtils.hashPassword(this.password)
  return next()
})

interface IUser extends Document {
  _id: string
  id: string
  username: string
  email: string
  recordCollection: string
  password: string
  activationAccountCode: string
  emailConfirmed: boolean
  picture?: string
  postsSaved: string[]
}

const UserModel = mongoose.model<IUser>('User', UserSchema)

export { UserModel, IUser }
