import { nanoid } from 'nanoid'

import { CollectionService } from 'features/collections'
import { AuthUtils } from 'utils/auth'

import { UserModel, IUser } from '../models'
import { CreateUser, UpdateUser } from '../types'

const findById = async (id: string) => {
  return UserModel.findOne({ _id: id })
}

const findByUsername = async (username: string) => {
  return UserModel.findOne({ username })
}

const findByEmail = async (email: string) => {
  return UserModel.findOne({ email })
}

const findByUsernameOrEmail = async (username: string) => {
  return UserModel.findOne({ $or: [{ email: username }, { username }] }).exec()
}

const create = async (user: CreateUser): Promise<IUser> => {
  const collection = await CollectionService.create()
  const newUser = { ...user, ...{ recordCollection: collection.id } }
  const userModel = new UserModel(newUser)
  return userModel.save()
}

const update = async (id: string, user: UpdateUser) => {
  return UserModel.findByIdAndUpdate(id, user)
}

const deleteById = async (id: string) => {
  return UserModel.deleteOne({ _id: id })
}

const changePassword = async (idUser: string, newPassword: string) => {
  const password = await AuthUtils.hashPassword(newPassword)
  return UserModel.findByIdAndUpdate(idUser, { password })
}

const activateUser = async (username: string, code: string) => {
  return UserModel.findOneAndUpdate(
    {
      $and: [{ activationAccountCode: code }, { $or: [{ email: username }, { username }] }],
    },
    { emailConfirmed: true },
  )
}

const updateActivationCode = async (username: string) => {
  return UserModel.findOneAndUpdate(
    {
      $or: [{ email: username }, { username }],
    },
    { activationAccountCode: nanoid(6) },
    { new: true },
  )
}

export const UserService = {
  findById,
  findByUsername,
  findByUsernameOrEmail,
  findByEmail,
  create,
  update,
  deleteById,
  changePassword,
  activateUser,
  updateActivationCode,
}
