export interface User {
  _id: string
  username: string
  email: string
  collection: string
  password: string
  picture?: string
}

export type CreateUser = Omit<User, '_id' | 'collection'>

export type UpdateUser = Partial<Omit<User, '_id'>>
