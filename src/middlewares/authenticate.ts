import passport from 'passport'

import { AuthenticationEnum } from 'types/authentication'

const authenticate = (type: AuthenticationEnum = AuthenticationEnum.JWT) =>
  passport.authenticate(type, { session: false })

export { authenticate }
