/* eslint-disable @typescript-eslint/no-unused-vars */
import { Request, Response, NextFunction } from 'express'
import mongoose from 'mongoose'

import { DuplicateKeyError, GenericError, ValidationError } from 'errors'
import { ERRORS } from 'consts'
import { Error } from 'types'

const isValidationError = (error: Error) => {
  return error instanceof mongoose.Error.ValidationError
}

const isDuplicateKeyError = (error: Error) => {
  return error.name === 'MongoError' && error.code === 11000
}

const getResponseError = (error: Error) => {
  if (isValidationError(error)) return new ValidationError(error.message)

  if (isDuplicateKeyError(error)) return new DuplicateKeyError(Object.keys(error.keyValue)[0])

  const commonError = error as Error
  if (commonError instanceof GenericError) return commonError

  return new GenericError(500, commonError.message || ERRORS.VALIDATION_ERROR)
}

const errorHandler = (error: Error, req: Request, res: Response, next: NextFunction): void => {
  const responseError = getResponseError(error)
  res
    .status(responseError.status)
    .send({ error: { ...responseError, message: responseError.message } })
}

export { errorHandler }
