import { Request, Response, NextFunction } from 'express'

export const parseJSON = (key: string) => (req: Request, res: Response, next: NextFunction) => {
  // eslint-disable-next-line no-param-reassign
  req.body[key] = JSON.parse(req.body[key])
  next()
}
