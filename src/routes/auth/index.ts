import { Router, Request, Response, NextFunction } from 'express'

import { GenericError } from 'errors'
import { UserService } from 'features/users/services'
import { authenticate } from 'middlewares/authenticate'
import { Mailer } from 'services'
import { AuthenticationEnum } from 'types'
import { AuthUtils } from 'utils/auth'
import { ERRORS } from 'consts'

const authRouter = Router()

const handleLogin = async (req: Request, res: Response, next: NextFunction) => {
  try {
    const authenticatedUser = req.user!
    const token = AuthUtils.generateJwt(authenticatedUser)
    const user = await UserService.findById(authenticatedUser.id)

    if (!user?.emailConfirmed) {
      next(new GenericError(400, 'Account not activated', ERRORS.ACCOUNT_NOT_CONFIRMED_ERROR))
      return
    }

    res.status(200).send({ user, token })
  } catch (error) {
    next(error)
  }
}

authRouter.post('/login/user', authenticate(AuthenticationEnum.UserLogin), handleLogin)

const sendActivationCodeEmail = (email: string, code: string) => {
  Mailer.sendMail(
    email,
    'Activación cuenta Record Collection',
    `Activá tu cuenta ingresando el siguente codigo ${code} en nuestra app.`,
  )
}

authRouter.post('/signup', async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { username, email, password } = req.body
    const user = await UserService.create({ username, email, password })

    sendActivationCodeEmail(user.email, user.activationAccountCode)

    res.status(200).send('User created')
  } catch (error) {
    next(error)
  }
})

authRouter.post('/changePassword', async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { newPassword, idUser } = req.body
    const user = await UserService.changePassword(idUser, newPassword)
    res.status(200).send({ user })
  } catch (error) {
    next(error)
  }
})

authRouter.post('/activateAccount', async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { username, code } = req.body
    const user = await UserService.activateUser(username, code)

    if (!user) {
      next(new GenericError(400, 'Incorrect activation code', 'IncorrectActivationCode'))
      return
    }
    res.status(200).send('Account activate')
  } catch (error) {
    next(error)
  }
})

authRouter.post(
  '/resendActivationCode',
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { username } = req.body
      const user = await UserService.updateActivationCode(username)

      if (user) {
        sendActivationCodeEmail(user.email, user.activationAccountCode)
      }

      res.status(200).send('User created')
    } catch (error) {
      next(error)
    }
  },
)

authRouter.post(
  '/login/token',
  authenticate(),
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const authenticatedUser = req.user!
      const token = AuthUtils.generateJwt(authenticatedUser)
      const user = await UserService.findById(authenticatedUser.id)
      res.send({ user, token })
    } catch (error) {
      next(error)
    }
  },
)

export { authRouter }
