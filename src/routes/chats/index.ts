import { Router, Request, Response, NextFunction } from 'express'

import { ChatService } from 'features/chats'
import { authenticate } from 'middlewares/authenticate'

const chatsRouter = Router()

chatsRouter.use(authenticate())

chatsRouter.get('/', async (req: Request, res: Response, next: NextFunction) => {
  const { user } = req
  try {
    const chats = await ChatService.getChatsByUserId(user!.id)
    res.json({ chats })
  } catch (error) {
    next(error)
  }
})

chatsRouter.get('/:id', async (req: Request, res: Response, next: NextFunction) => {
  const { id } = req.params
  try {
    const chat = await ChatService.getOne(id)
    res.json({ chat })
  } catch (error) {
    next(error)
  }
})

chatsRouter.post('/', async (req: Request, res: Response, next: NextFunction) => {
  const { user } = req
  const { userId } = req.body
  try {
    const chat = await ChatService.create({ user1: user!.id, user2: userId })
    res.json({ chat })
  } catch (error) {
    next(error)
  }
})

chatsRouter.put('/:id/messages', async (req: Request, res: Response, next: NextFunction) => {
  const { user } = req
  const { id } = req.params
  const { message } = req.body

  try {
    await ChatService.addMessage(id, message, user!)
    res.sendStatus(200)
  } catch (error) {
    next(error)
  }
})

export { chatsRouter }
