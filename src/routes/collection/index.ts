import { Router, Request, Response, NextFunction } from 'express'

import { CollectionService } from 'features/collections'
import { AddReleasePayload } from 'features/collections/types'
import { authenticate } from 'middlewares/authenticate'

const collectionRouter = Router()

collectionRouter.use(authenticate())

collectionRouter.get(
  '/:idCollection/wishList',
  async (req: Request, res: Response, next: NextFunction) => {
    const { search } = req.query
    const { idCollection } = req.params
    try {
      const [wishList] = await CollectionService.getWishList(
        idCollection,
        search ? search.toString() : '',
      )
      res.json(wishList)
    } catch (error) {
      next(error)
    }
  },
)

collectionRouter.post('/wishList', async (req: Request, res: Response, next: NextFunction) => {
  const payload = req.body as AddReleasePayload
  try {
    await CollectionService.addReleaseToWishList(payload)
    res.sendStatus(200)
  } catch (error) {
    next(error)
  }
})

collectionRouter.delete(
  '/wishList/:collectionId/:releaseId',
  async (req: Request, res: Response, next: NextFunction) => {
    const { collectionId, releaseId } = req.params
    try {
      await CollectionService.removeReleaseFromWishList(collectionId, Number(releaseId))
      res.sendStatus(200)
    } catch (error) {
      next(error)
    }
  },
)

export { collectionRouter }
