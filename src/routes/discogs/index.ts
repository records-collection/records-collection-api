import { Router, Request, Response, NextFunction } from 'express'

import { authenticate } from 'middlewares'
import { DiscogsService } from 'features/discogs'

const discogsRouter = Router()

discogsRouter.use(authenticate())

discogsRouter.get('/releases/:id', async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { id } = req.params
    const { data } = await DiscogsService.findReleaseById(id)
    res.status(200).send(data)
  } catch (error) {
    // TODO: handle discogs errors
    next(error)
  }
})

discogsRouter.get('/artists/:id', async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { id } = req.params
    const { data: artist } = await DiscogsService.findArtistById(id)
    res.status(200).send({ artist })
  } catch (error) {
    // TODO: handle discogs errors
    next(error)
  }
})

discogsRouter.get(
  '/artists/:id/releases',
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { id } = req.params
      const { data } = await DiscogsService.findReleasesByArtistId(id)
      res.status(200).send(data)
    } catch (error) {
      // TODO: handle discogs errors
      next(error)
    }
  },
)

discogsRouter.get(
  '/artists/:id/masters',
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const { id } = req.params
      const { data } = await DiscogsService.findMastersByArtistId(id)
      res.status(200).send(data)
    } catch (error) {
      // TODO: handle discogs errors
      next(error)
    }
  },
)

discogsRouter.get('/masters/:id', async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { id } = req.params
    const { data } = await DiscogsService.findMasterById(id)
    res.status(200).send({ master: data })
  } catch (error) {
    // TODO: handle discogs errors
    next(error)
  }
})

discogsRouter.get(
  '/versions/:masterId',
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      // TODO: validar req.query
      const { masterId } = req.params
      const { data } = await DiscogsService.findVersionsByMasterId(masterId, req.query)
      res.status(200).send(data)
    } catch (error) {
      // TODO: handle discogs errors
      next(error)
    }
  },
)

discogsRouter.get('/search', async (req: Request, res: Response, next: NextFunction) => {
  try {
    // TODO: validar req.query
    const { query, type, pageSize, page, year, format, country } = req.query
    const { data } = await DiscogsService.search(
      query as string,
      type as string,
      year as string,
      format as string,
      country as string,
      Number(pageSize),
      Number(page),
    )
    res.status(200).send(data)
  } catch (error) {
    // TODO: handle discogs errors
    next(error)
  }
})

discogsRouter.get('/search', async (req: Request, res: Response, next: NextFunction) => {
  try {
    // TODO: validar req.query
    const { query, type, pageSize, page, year, format, country } = req.query
    const { data } = await DiscogsService.search(
      query as string,
      type as string,
      year as string,
      format as string,
      country as string,
      Number(pageSize),
      Number(page),
    )
    res.status(200).send(data)
  } catch (error) {
    // TODO: handle discogs errors
    next(error)
  }
})

export { discogsRouter }
