import express from 'express'

import { releasesRouter } from './releases'
import { discogsRouter } from './discogs'
import { authRouter } from './auth'
import { collectionRouter } from './collection'
import { postsRouter } from './posts'
import { chatsRouter } from './chats'

const routes = express.Router()

routes.use('/releases', releasesRouter)
routes.use('/discogs', discogsRouter)
routes.use('/auth', authRouter)
routes.use('/collection', collectionRouter)
routes.use('/posts', postsRouter)
routes.use('/chats', chatsRouter)

export { routes }
