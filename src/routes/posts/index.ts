import { Router, Request, Response, NextFunction } from 'express'

import { PostService } from 'features/posts'
import { authenticate } from 'middlewares/authenticate'
import { CreateCommentPayload, CreatePostPayload, UpdatePostPayload } from 'features/posts/types'
import { parseJSON, uploadFile } from 'middlewares'

import { PostSerilizer } from './serializer'

const postsRouter = Router()

postsRouter.use(authenticate())

postsRouter.get('/', async (req: Request, res: Response, next: NextFunction) => {
  try {
    const result = await PostService.getAll()
    const posts = await PostSerilizer.serializeList(result)
    res.json({ posts })
  } catch (error) {
    next(error)
  }
})

postsRouter.get('/:id', async (req: Request, res: Response, next: NextFunction) => {
  const { id } = req.params
  try {
    const postResult = await PostService.getOne(id)
    const post = await PostSerilizer.serialize(postResult)
    res.json({ post })
  } catch (error) {
    next(error)
  }
})

postsRouter.post(
  '/',
  uploadFile.array('images', 10),
  parseJSON('post'),
  async (req: Request, res: Response, next: NextFunction) => {
    const { user } = req
    const images = req.files
    const payload = req.body.post as CreatePostPayload

    try {
      const post = await PostService.create({ ...payload, user: user!.id }, images)
      res.json({ post })
    } catch (error) {
      next(error)
    }
  },
)

postsRouter.put(
  '/:id',
  uploadFile.array('images', 10),
  parseJSON('post'),
  async (req: Request, res: Response, next: NextFunction) => {
    const { id } = req.params
    const images = req.files
    const { user } = req
    const payload = req.body.post as UpdatePostPayload

    try {
      const post = await PostService.update(id, payload, user!, images)
      res.json({ post })
    } catch (error) {
      next(error)
    }
  },
)

postsRouter.delete('/:id', async (req: Request, res: Response, next: NextFunction) => {
  const { id } = req.params
  try {
    await PostService.deleteOne(id)
    res.sendStatus(200)
  } catch (error) {
    next(error)
  }
})

postsRouter.post('/:id/comments', async (req: Request, res: Response, next: NextFunction) => {
  const { id } = req.params
  const { user } = req
  const payload = req.body as CreateCommentPayload
  try {
    await PostService.createComment(id, payload, user!)
    res.sendStatus(200)
  } catch (error) {
    next(error)
  }
})

postsRouter.put('/:id/like', async (req: Request, res: Response, next: NextFunction) => {
  const { id } = req.params
  const { user } = req
  try {
    await PostService.like(id, user!)
    res.sendStatus(200)
  } catch (error) {
    next(error)
  }
})

postsRouter.put('/:id/unlike', async (req: Request, res: Response, next: NextFunction) => {
  const { id } = req.params
  const { user } = req
  try {
    await PostService.unlike(id, user!)
    res.sendStatus(200)
  } catch (error) {
    next(error)
  }
})

postsRouter.put('/:id/reserve', async (req: Request, res: Response, next: NextFunction) => {
  const { id } = req.params
  const { user } = req
  try {
    const post = await PostService.reserve(id, user!)
    res.json({ post })
  } catch (error) {
    next(error)
  }
})

postsRouter.put('/:id/save', async (req: Request, res: Response, next: NextFunction) => {
  const { id } = req.params
  const { user } = req
  try {
    const post = await PostService.save(id, user!)
    res.json({ post })
  } catch (error) {
    next(error)
  }
})

postsRouter.put('/:id/removeSaved', async (req: Request, res: Response, next: NextFunction) => {
  const { id } = req.params
  const { user } = req
  try {
    const post = await PostService.removeSaved(id, user!)
    res.json({ post })
  } catch (error) {
    next(error)
  }
})

postsRouter.get('/user/saved', async (req: Request, res: Response, next: NextFunction) => {
  const { user } = req
  try {
    const result = await PostService.postsSaved(user!)
    res.json({ posts: result.postsSaved })
  } catch (error) {
    next(error)
  }
})

postsRouter.get('/user/myposts', async (req: Request, res: Response, next: NextFunction) => {
  const { user } = req
  const { state } = req.query
  try {
    const posts = await PostService.getAllByUser(user!.id, state as string)
    res.json({ posts })
  } catch (error) {
    next(error)
  }
})

export { postsRouter }
