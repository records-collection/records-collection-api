import { IPost } from 'features/posts/models'
import { ReleaseSerializer } from 'routes/releases/serializer'

const serialize = async (post: IPost) => {
  const postSerialized = { ...JSON.parse(JSON.stringify(post)) }
  const release = await ReleaseSerializer.serialize(post.release)

  return { ...postSerialized, release }
}

const serializeList = async (posts: IPost[]) => {
  return Promise.all(posts.map(async (post) => serialize(post)))
}

export const PostSerilizer = { serialize, serializeList }
