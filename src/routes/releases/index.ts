import { Router, Request, Response, NextFunction } from 'express'

import { ReleaseService } from 'features/releases'
import { CreateRelease, UpdateRelease } from 'features/releases/types'
import { parseJSON, uploadFile } from 'middlewares'
import { authenticate } from 'middlewares/authenticate'

import { ReleaseSerializer } from './serializer'

const releasesRouter = Router()

releasesRouter.use(authenticate())

releasesRouter.get('/', async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { collectionId, size, page, search } = req.query

    // TODO: add types
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const query: any = {
      collectionId,
      $or: [
        { title: { $regex: search ?? '', $options: 'i' } },
        { 'artists.name': { $regex: search ?? '', $options: 'i' } },
      ],
    }

    const releases = await ReleaseService.findAllByCollectionId(
      collectionId as string,
      Number(size) ?? 10,
      Number(page) ?? 0,
      query,
    )
    const count = await ReleaseService.count(query)
    const releasesSerialized = await ReleaseSerializer.serializeList(releases)
    res.send({ count, releases: releasesSerialized })
  } catch (error) {
    next(error)
  }
})

releasesRouter.get('/:id', async (req: Request, res: Response, next: NextFunction) => {
  try {
    const { id } = req.params
    const release = await ReleaseService.findById(id)
    const releaseSerialized = await ReleaseSerializer.serialize(release)
    res.send({ release: releaseSerialized })
  } catch (error) {
    next(error)
  }
})

releasesRouter.post(
  '/',
  uploadFile.array('images', 10),
  parseJSON('release'),
  async (req: Request, res: Response, next: NextFunction) => {
    const createRelease = req.body.release as CreateRelease

    const images = req.files

    try {
      const release = await ReleaseService.create(createRelease, images)
      res.send({ release })
    } catch (error) {
      next(error)
    }
  },
)

releasesRouter.put(
  '/:id',
  uploadFile.array('images', 10),
  parseJSON('release'),
  async (req: Request, res: Response, next: NextFunction) => {
    const { id } = req.params
    const images = req.files
    const updateRelease = req.body.release as UpdateRelease
    try {
      const release = await ReleaseService.update(id, updateRelease, images)
      res.send({ release })
    } catch (error) {
      next(error)
    }
  },
)

releasesRouter.delete('/:id', async (req: Request, res: Response, next: NextFunction) => {
  const { id } = req.params
  try {
    await ReleaseService.deleteById(id)
    res.sendStatus(200)
  } catch (error) {
    next(error)
  }
})

export { releasesRouter }
