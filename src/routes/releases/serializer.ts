import { AmazonS3Service } from 'features/amazon'
import { IRelease } from 'features/releases/models'

const serialize = async (release: IRelease) => {
  const ownImages = await Promise.all(
    release.imagesKeys?.map(async (key) => ({ uri: await AmazonS3Service.getSignedUrl(key), key })),
  )

  return { ...JSON.parse(JSON.stringify(release)), ownImages }
}

const serializeList = async (releases: IRelease[]) => {
  return Promise.all(
    releases.map(async (release) => {
      if (release.imagesKeys?.length) {
        const ownImage = await AmazonS3Service.getSignedUrl(release.imagesKeys[0])
        return {
          ...JSON.parse(JSON.stringify(release)),
          ownImage: { uri: ownImage, key: release.imagesKeys[0] },
        }
      }

      return { ...JSON.parse(JSON.stringify(release)) }
    }),
  )
}

export const ReleaseSerializer = { serialize, serializeList }
