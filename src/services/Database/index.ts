import mongoose from 'mongoose'
import config from 'config'

const dbHost: string = config.get('dbHost')

const options = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  autoIndex: true,
  useCreateIndex: true,
}

const connect = (): void => {
  mongoose.connect(dbHost, options).then(
    () => {
      // eslint-disable-next-line no-console
      console.log('Database is connected')
    },
    (err: string) => {
      // eslint-disable-next-line no-console
      console.log(`Can not connect to the database: ${err}`)
    },
  )
}

export const Database = {
  connect,
}
