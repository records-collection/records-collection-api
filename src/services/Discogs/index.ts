import axios from 'axios'
import config from 'config'

const { apiUrl, consumerKey, consumerSecretKey } = config.get('discogs')

const Discogs = axios.create({
  baseURL: apiUrl,
  headers: {
    Authorization: `Discogs key=${consumerKey}, secret=${consumerSecretKey}`,
  },
})

export { Discogs }
