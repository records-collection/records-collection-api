import config from 'config'
import nodemailer from 'nodemailer'
import smtpTransport from 'nodemailer-smtp-transport'

const { server, user, password } = config.get('smtp')

const transporter = nodemailer.createTransport(
  smtpTransport({
    service: 'gmail',
    host: server,
    auth: {
      user,
      pass: password,
    },
  }),
)

const sendMail = (to: string, subject: string, text: string) => {
  const mailOptions = {
    from: 'register@recordcollection.com',
    to,
    subject,
    text,
  }
  transporter.sendMail(mailOptions, (error, info) => {
    if (error) {
      // eslint-disable-next-line no-console
      console.log(error)
    } else {
      // eslint-disable-next-line no-console
      console.log(`Email sent:${info.response}`)
    }
  })
}

export const Mailer = {
  sendMail,
}
