import passport from 'passport'

import { AuthenticationEnum, JWTEnum } from 'types/authentication'

import { localStrategy, jwtStrategy } from './strategies'

const config = () => {
  passport.use(AuthenticationEnum.UserLogin, localStrategy())

  passport.use(AuthenticationEnum.JWT, jwtStrategy(JWTEnum.Authentication))
}

export const Passport = {
  config,
}
