import envConfig from 'config'
import { ExtractJwt, Strategy as JwtStrategy } from 'passport-jwt'

import { AuthenticationService } from 'features/auth'
import { JWTEnum } from 'types/authentication'

const jwtStrategy = (jwtType: JWTEnum) =>
  new JwtStrategy(
    {
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      secretOrKey: envConfig.get('jwtSecret'),
    },
    async (payload, done) => {
      try {
        if (payload.jwtType !== jwtType) throw new Error('Unauthorired error')
        const user = await AuthenticationService.findAuthenticatedUser(payload.id)

        return done(null, user)
      } catch (error) {
        return done(error, null)
      }
    },
  )

export { jwtStrategy }
