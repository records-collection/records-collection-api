import { Strategy as LocalStrategy } from 'passport-local'

import { AuthenticationService } from 'features/auth'

const localStrategy = () =>
  new LocalStrategy(
    { usernameField: 'username', passReqToCallback: true, session: false },
    async (_req, username, password, done) => {
      try {
        const user = await AuthenticationService.authenticate(username, password)
        done(null, user)
      } catch (error) {
        done(error, false)
      }
    },
  )

export { localStrategy }
