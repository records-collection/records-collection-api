import mongoose from 'mongoose'

import { ReleaseModel } from 'features/releases/models'
import { CreateRelease } from 'features/releases/types'

const release: CreateRelease = {
  collectionId: '5f80cf4e1db52d69954a0fc1',
  userId: '5f80cf4e1db52d69954a0fc1',
  title: 'Never Gonna Give You Up',
  discogsId: 249504,
  images: [
    {
      height: 600,
      resourceUrl:
        'https://img.discogs.com/6bm5HoKZiGR3kVLPpfwws6Ou7ms=/fit-in/600x600/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-249504-1334592238.jpeg.jpg',
      type: 'primary',
      uri:
        'https://img.discogs.com/6bm5HoKZiGR3kVLPpfwws6Ou7ms=/fit-in/600x600/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-249504-1334592238.jpeg.jpg',
      uri150:
        'https://api-img.discogs.com/0ZYgPR4X2HdUKA_jkhPJF4SN5mM=/fit-in/150x150/filters:strip_icc():format(jpeg):mode_rgb()/discogs-images/R-249504-1334592212.jpeg.jpg',
      width: 600,
    },
  ],
  labels: [
    {
      catno: 'PB 41447',
      discogsId: 895,
      name: 'RCA',
      resourceUrl: 'https://api.discogs.com/labels/895',
    },
  ],
  year: 1987,
  country: 'UK',
  artists: [
    {
      anv: '',
      discogsId: 72872,
      join: '',
      name: 'Rick Astley',
      resourceUrl: 'https://api.discogs.com/artists/72872',
      role: '',
      tracks: '',
    },
  ],
  genres: ['Electronic', 'Pop'],
  identifiers: [
    {
      type: 'Barcode',
      value: '5012394144777',
    },
  ],
  formats: [
    {
      name: 'name',
      qty: '1',
      descriptions: ['description'],
    },
  ],
}

describe('Release Model Test', () => {
  beforeAll(async () => {
    await mongoose.connect(
      global.__MONGO_URI__,
      { useNewUrlParser: true, useCreateIndex: true, useUnifiedTopology: true },
      (err) => {
        if (err) {
          process.exit(1)
        }
      },
    )
  })

  it('should create and save release successfully', async () => {
    const validRelease = new ReleaseModel(release)
    const savedRelease = await validRelease.save()

    expect(savedRelease._id).toBeDefined()
    expect(savedRelease.title).toBe(release.title)
    expect(savedRelease.artists[0].name).toBe(release.artists[0].name)
  })

  it('should update a release successfully', async () => {
    const updatePayload = { personalNotes: 'personalNotes', supportState: 'VG', mediaState: 'VG+' }
    const validRelease = new ReleaseModel(release)
    const savedRelease = await validRelease.save()

    expect(savedRelease._id).toBeDefined()
    expect(savedRelease.title).toBe(release.title)

    const updateRelease = await ReleaseModel.findByIdAndUpdate(savedRelease._id, updatePayload, {
      new: true,
    })

    expect(updateRelease?._id).toBeDefined()
    expect(updateRelease?.personalNotes).toBe(updatePayload.personalNotes)
    expect(updateRelease?.supportState).toBe(updatePayload.supportState)
    expect(updateRelease?.mediaState).toBe(updatePayload.mediaState)
  })

  it('should delete a release successfully', async () => {
    const validRelease = new ReleaseModel(release)
    const savedRelease = await validRelease.save()

    expect(savedRelease._id).toBeDefined()
    expect(savedRelease.title).toBe(release.title)

    await ReleaseModel.findByIdAndDelete(savedRelease._id)

    const deletedRelease = await ReleaseModel.findById(savedRelease._id)

    expect(deletedRelease).toBeNull()
  })

  it('should not create a release whithout required title', async () => {
    const releaseWithoutTitle = { ...release, title: null }
    const userWithoutRequiredField = new ReleaseModel(releaseWithoutTitle)
    let err
    try {
      const savedUserWithoutRequiredField = await userWithoutRequiredField.save()
      err = savedUserWithoutRequiredField
    } catch (error) {
      err = error
    }
    expect(err).toBeInstanceOf(mongoose.Error.ValidationError)
    expect(err.errors.title).toBeDefined()
  })

  it('should not create a release whithout required collectionId', async () => {
    const releaseWithoutTitle = { ...release, collectionId: null }
    const userWithoutRequiredField = new ReleaseModel(releaseWithoutTitle)
    let err
    try {
      const savedUserWithoutRequiredField = await userWithoutRequiredField.save()
      err = savedUserWithoutRequiredField
    } catch (error) {
      err = error
    }
    expect(err).toBeInstanceOf(mongoose.Error.ValidationError)
    expect(err.errors.collectionId).toBeDefined()
  })

  it('should not create a release whithout required userId', async () => {
    const releaseWithoutTitle = { ...release, userId: null }
    const userWithoutRequiredField = new ReleaseModel(releaseWithoutTitle)
    let err
    try {
      const savedUserWithoutRequiredField = await userWithoutRequiredField.save()
      err = savedUserWithoutRequiredField
    } catch (error) {
      err = error
    }
    expect(err).toBeInstanceOf(mongoose.Error.ValidationError)
    expect(err.errors.userId).toBeDefined()
  })

  afterAll(async (done) => {
    await mongoose.connection.close()
    done()
  })
})
