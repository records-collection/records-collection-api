export enum AuthenticationEnum {
  UserLogin = 'userLogin',
  JWT = 'jwt',
}

export enum JWTEnum {
  Authentication = 'Authentication',
}
