export interface Error {
  code: number
  message: string
  name?: string
  stack?: string
  // TODO: add type
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  keyValue?: any
}
