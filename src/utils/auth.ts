import bcrypt from 'bcrypt'
import config from 'config'
import jwt, { SignOptions } from 'jsonwebtoken'

import { AuthenticatedUser, JWTEnum } from 'types'

const generateJwt = (
  payload: AuthenticatedUser,
  type: JWTEnum = JWTEnum.Authentication,
  options?: SignOptions,
) => {
  return jwt.sign({ ...payload, jwtType: type }, config.get('jwtSecret'), options)
}

const hashPassword = async (password: string) => {
  return bcrypt.hash(password, 10)
}

const comparePassword = async (password: string, hash: string) => {
  return bcrypt.compare(password, hash)
}

export const AuthUtils = {
  generateJwt,
  hashPassword,
  comparePassword,
}
